Instruction to run asterism for morphology 
============================================


Download data
-----------------------------------
download the data from [download link][smallsim]

* untar the file sersic_CAS.tar.gz it will create a directory named `sersic`


Preparing data
-----------------------------------
build the images and seg_map cubes


* `asterism_build_fits_cube blended_cube.fist  sersic/BLENDED_0*.fits`

* `asterism_build_fits_cube blended_seg_map_cube.fist ../sersic/BLENDED_SEGMAP_00*.fits`


Using the asterism script
-----------------------------------
* get the `shape_test.conf` fro this page of the repository

* run the command line

  	-`asterism_gal_shape_features_euclid blended_cube.fist shape_test.conf -input_seg_map blended_seg_map_cube.fist -save_products`

    -**do not forget the -save_products option**, otherwise you will have no morphology catalo as output


* to run on a sub_set of images, for example from `image_id=25` to `image_id=100` 
  
	-`asterism_gal_shape_features_euclid blended_cube.fist shape_test.conf -input_seg_map blended_seg_map_cube.fist -save_products -image_id 25 -image_id 100`	

* the morphology catalog will be stored in the file `morph_features.fits`

	* `images_id` column refer to the position of the image in the cube  **starting from 0**
	* `id_cluster`  refers the same ID in the `seg_map`, for the central source,  **-1** refers to no source detected, it happens for 3 sources (the segmap is empty)
	* the columns you are interested are :
	  -`morph_Gini_ic`
	  -`morph_M20_ic`
	  -`morph_conc_1_ic`
	  -`morph_conc_2_ic`
	  -`morph_asymm_ic`
	  -`morph_clumpiness_ic`
	
* you can run a very simple analysis using the notebock `analysis.ipynb`


 
 





[smallsim]: https://drive.google.com/drive/folders/1jqxJMi2pJHnyZKc5KSxxyJBiTZwetCkA?usp=sharing
