Asterism notes for Morphology
==================================

There are several ways to run a Morphology extraction with asterism,
we illustrate two

  * using the asterism script: `asterism_gal_shape_features_euclid`
  * using a custom python [script](asterism_run.py)

Using the asterism script
-----------------------------------
* The typical command line is

  `asterism_gal_shape_features_euclid image_file conf_file -save_products`

  * `image_file` can be either a single image ora a cube

  * `conf_file` is given [here](shape_test.conf)

    **VERY IMPORTANT**:

    in the conf_file, in the section `source_catalog` the following setting `min_pix_size = 1000`
    is used to remove all the sources with less than 1000 pixels size, this is done only to seed-up the demo, set it to `None` for complete processing

  * a default conf_file can be obtained typing:

   `asterism_gal_shape_features_euclid foo -dump_conf_file conf.test`

   where `conf.test` is the arbitrary name of the conf_file

  * `-save_products` will write to file the output of asterism

  * `asterism_gal_shape_features_euclid - h` provides help for the command line

* If you want to use a segmentation map you have to
  1. add  the command line `-input_seg_map_file seg_map.fit`

    where ` seg_map.fit` is the arbitrary file name
  2. edit the conf_file section:      `[ task: image_segmentation: start]`

      setting:

      `method = from_seg_map`



Using the custom python script
---------------------------------------------------
 * The typical command line is
  `asterism_run.py image_file conf_file -input_seg_map_file seg_map.fit`
   where ` seg_map.fit` is the arbitrary file name

* `image_file` can be single file a cube or list (using wild card) of both
of them
