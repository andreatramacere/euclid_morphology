Asterism Morphology code benchmark
==================================
General information
-------------------
In this repository are stored the scripts to run Asterism for  the
extraction of Morphology features

We will investigate the following parameters:

  * CAS (Concentration - Asymmetry - Smoothness)
  * $`M_{20}`$
  * Gini coefficient

The testing material
--------------------
* Marc  sersic simulated images
      ([download link][smallsim], ~1800 images)


The Asterism framework
----------------------
* The code to run Asterism can be found [here](asterism)
and the instructions [here](asterism/README.md)

* The Asterism repository is [here](https://gitlab.com/andrea.tramacere/asterism)

* The Morphology algorithms are implemented in:  
`asterism.core.morphometry.non_parametric.morphology.py`

* The Petrosian radius depends on the estimation of the BrigthnessRadialProfile   
`asterism.core.photometry.surface_brightness_profile.ImageBrigthnessRadialProfile`


[smallsim]: https://drive.google.com/drive/folders/1jqxJMi2pJHnyZKc5KSxxyJBiTZwetCkA?usp=sharing
